﻿using System;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Collections.Generic;
using System.Data;

namespace Data
{
    public class DBConnection
    {
        private DBConnection ()
        {
        }

        private string databaseServer = string.Empty;
        public string DatabaseServer
        {
            get { return databaseServer; }
            set { databaseServer = value; }
        }

        private string databaseName = string.Empty;
        public string DatabaseName
        {
            get { return databaseName; }
            set { databaseName = value; }
        }

        public string Uuid { get; set; }
        public string Password { get; set; }

        private MySqlConnection connection = null;
        public MySqlConnection Connection
        {
            get { return connection; }
        }

        private static DBConnection _instance = null;

        public static DBConnection Instance ()
        {
            if (_instance == null)
                _instance = new DBConnection();
            return _instance;
        }

        public bool IsConnect ()
        {
            bool result = true;

            if (String.IsNullOrEmpty(databaseServer) || String.IsNullOrEmpty(databaseName) ||
                String.IsNullOrEmpty(Uuid) || String.IsNullOrEmpty(Password))
            {
                result = false;
            }
            else
            {
                if (Connection == null)
                {
                    string connstring = string.Format("Server={0}; database={1}; UID={2}; password={3};  AllowUserVariables=true", databaseServer, databaseName, Uuid, Password);
                    connection = new MySqlConnection(connstring);
                }

                if (Connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                    result = true;
                }
            }

            return result;
        }

        public void Close ()
        {
            connection.Close();
            connection = null;
        }
    }

    public class Parameters
    {
        private List<String> listParams = new List<string>();
        public List<String> ListParams
        {
            get { return listParams; }
            set { listParams = value; }
        }

        public void Add (string ParamName, string ParamValue)
        {
            if (ParamName.Substring(0, 1) != "?")
                ParamName = "?" + ParamName;

            listParams.Add(ParamName + "=" + ParamValue);
        }
        public void Add (string ParamName, int ParamValue)
        {
            if (ParamName.Substring(0, 1) != "?")
                ParamName = "?" + ParamName;

            listParams.Add(ParamName + "=" + ParamValue.ToString());
        }
        public void Add (string ParamName, bool ParamValue)
        {
            if (ParamName.Substring(0, 1) != "?")
                ParamName = "?" + ParamName;

            listParams.Add(ParamName + "=" + (ParamValue ? "1" : "0"));
        }
        public void Add (string ParamName, DateTime ParamValue)
        {
            if (ParamName.Substring(0, 1) != "?")
                ParamName = "?" + ParamName;

            listParams.Add(ParamName + "=" + ParamValue.ToString("yyyy-MM-dd HH:mm:ss"));
        }
        public void Add (string ParamName, Decimal ParamValue, uint Precision)
        {
            if (ParamName.Substring(0, 1) != "?")
                ParamName = "?" + ParamName;

            listParams.Add(ParamName + "=" + ParamValue.ToString("N" + Precision.ToString()).Replace(".", "").Replace(",", "."));
        }
    }

    public class DisconnectedOperations
    {
        private DBConnection connection = null;

        public DBConnection Connection
        {
            get { return connection; }
            set { connection = value; }
        }


        public DisconnectedOperations ()
        {
            connection = DBConnection.Instance();
            connection.DatabaseServer = ConfigurationManager.AppSettings["DatabaseServer"].ToString();
            connection.DatabaseName = ConfigurationManager.AppSettings["DatabaseName"].ToString();
            connection.Password = ConfigurationManager.AppSettings["DBPassword"].ToString();
            connection.Uuid = ConfigurationManager.AppSettings["DBUserName"].ToString();
        }

        public Int32 ExecuteCommand (string sql, List<string> parameters, CommandType type = CommandType.Text)
        {
            Int32 resultado = -1;

            try
            {
                if (Connection.IsConnect())
                {
                    using (var cmd = new MySqlCommand(sql, Connection.Connection))
                    {
                        cmd.CommandType = type;

                        if (parameters != null && parameters.Count > 0)
                        {
                            foreach (string parameter in parameters)
                            {
                                string[] p = parameter.Split('=');
                                cmd.Parameters.AddWithValue(p[0], p[1]);
                            }
                        }

                        cmd.ExecuteNonQuery();
                        cmd.CommandText = "SELECT LAST_INSERT_ID()";
                        resultado = Convert.ToInt32(cmd.ExecuteScalar());
                    }
                }
            }
            catch
            {
                resultado = -1;
            }

            return resultado;
        }
        public DataTable ExecuteSelectCommand (string sql, List<string> parameters, CommandType type = CommandType.Text)
        {
            DataTable resultado = new DataTable();

            try
            {
                if (Connection.IsConnect())
                {
                    using (var cmd = new MySqlCommand(sql, Connection.Connection))
                    {
                        cmd.CommandType = type;

                        if (parameters != null && parameters.Count > 0)
                        {
                            foreach (string parameter in parameters)
                            {
                                string[] p = parameter.Split('=');
                                cmd.Parameters.AddWithValue(p[0], p[1]);
                            }
                        }

                        MySqlDataReader reader = cmd.ExecuteReader();
                        resultado.Load(reader);
                        Connection.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }

            return resultado;
        }
    }
}
