﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CommonLib;

namespace EasyPDV
{
    public partial class CaixaSangria : Form
    {
        public delegate void CaixaSangriaUpdateHandler(object sender, CaixaSangriaUpdateEventArgs e);
        public event CaixaSangriaUpdateHandler CaixaSangriaUpdated;
        private Common com = new Common();

        public CaixaSangria()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            decimal valor = Convert.ToDecimal("0" + tbValor.Text.Replace(".", "").Trim());
            if (valor == 0)
                MessageBox.Show("Informe o valor sangrado do caixa", "Erro", MessageBoxButtons.OK);
            else
            {
                try
                {
                    CaixaSangriaUpdateEventArgs args = new CaixaSangriaUpdateEventArgs(valor);
                    CaixaSangriaUpdated(this, args);
                    this.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK);
                }
            }
        }

        private void maskedTextBox1_Enter(object sender, EventArgs e)
        {
            MaskedTextBox mt = (MaskedTextBox)sender;
            mt = com.RestoreMask(mt);
        }

        private void maskedTextBox1_Leave(object sender, EventArgs e)
        {
            MaskedTextBox mt = (MaskedTextBox)sender;
            mt = com.CompleteMask(mt);
        }

        private void maskedTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            MaskedTextBox mt = (MaskedTextBox)sender;
            if (e.KeyChar != (char)Keys.Back && char.IsNumber(e.KeyChar))
            {
                mt = com.EnterNumericValue(mt, e.KeyChar);
            }
        }

        private void maskedTextBox1_KeyUp(object sender, KeyEventArgs e)
        {
            MaskedTextBox mt = (MaskedTextBox)sender;
            if (e.KeyCode == Keys.Back)
            {
                mt = com.BackSpace(mt);
            }
        }

        private void CaixaSangria_Shown(object sender, EventArgs e)
        {
            tbValor.Focus();
        }

    }
    public class CaixaSangriaUpdateEventArgs : System.EventArgs
    {
        private Decimal mValorSaidas;

        public CaixaSangriaUpdateEventArgs(Decimal sValorSaidas)
        {
            this.mValorSaidas = sValorSaidas;
        }

        public Decimal ValorSaidas
        {
            get { return mValorSaidas; }
        }
    }
}
