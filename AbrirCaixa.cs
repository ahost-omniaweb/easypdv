﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EasyPDV
{
    public partial class AbrirCaixa : Form
    {
        public delegate void AbrirCaixaUpdateHandler(object sender, AbrirCaixaUpdateEventArgs e);
        public event AbrirCaixaUpdateHandler AbrirCaixaUpdated;

        public AbrirCaixa()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string valor = tbValor.Text.Replace(".", "").Trim();
            if (String.IsNullOrEmpty(valor) || Convert.ToDecimal(valor) == 0)
                MessageBox.Show("Informe o valor de abertura do caixa", "Erro", MessageBoxButtons.OK);
            else
            {
                AbrirCaixaUpdateEventArgs args = new AbrirCaixaUpdateEventArgs(Convert.ToDecimal(valor), true);
                AbrirCaixaUpdated(this, args);
                this.Dispose();
            }
        }
    }

    public class AbrirCaixaUpdateEventArgs : System.EventArgs
    {
        private Decimal mValorCaixa;
        private Boolean mCaixaAberto;

        public AbrirCaixaUpdateEventArgs ( Decimal sValorCaixa, Boolean sCaixaAberto)
        {
            this.mValorCaixa = sValorCaixa;
            this.mCaixaAberto = sCaixaAberto;
        }

        public Decimal ValorCaixa
        {
            get { return mValorCaixa; }
        }
        public Boolean CaixaAberto
        {
            get { return mCaixaAberto; }
        }
    }
}
