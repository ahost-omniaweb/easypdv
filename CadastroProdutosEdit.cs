﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CommonLib;

namespace EasyPDV
{
    public partial class CadastroProdutosEdit : Form
    {
        public delegate void CadastroProdutosUpdateHandler(object sender, CadastroProdutosUpdateEventArgs e);
        public event CadastroProdutosUpdateHandler CadastroProdutosUpdated;
        private Common com = new Common();

        public CadastroProdutosEdit()
        {
            InitializeComponent();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            decimal CustoMedio = Convert.ToDecimal("0" + tbCustoMedio.Text.Replace(".", "").Trim());
            decimal UltimoCusto = Convert.ToDecimal("0" + tbUltimoCusto.Text.Replace(".", "").Trim());
            decimal PrecoVenda = Convert.ToDecimal("0" + tbPrecoVenda.Text.Replace(".", "").Trim());
            if (CustoMedio == 0)
                MessageBox.Show("Informe o valor sangrado do caixa", "Erro", MessageBoxButtons.OK);
            else
            {
                try
                {
                    CadastroProdutosUpdateEventArgs args = new CadastroProdutosUpdateEventArgs(CustoMedio);
                    CadastroProdutosUpdated(this, args);
                    this.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK);
                }
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
    public class CadastroProdutosUpdateEventArgs : System.EventArgs
    {
        private Decimal mValorSaidas;

        public CadastroProdutosUpdateEventArgs(Decimal sValorSaidas)
        {
            this.mValorSaidas = sValorSaidas;
        }

        public Decimal ValorSaidas
        {
            get { return mValorSaidas; }
        }
    }
}
