﻿namespace EasyPDV
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.menuPrincipal = new System.Windows.Forms.MenuStrip();
            this.caixa = new System.Windows.Forms.ToolStripMenuItem();
            this.caixaAbrirCaixa = new System.Windows.Forms.ToolStripMenuItem();
            this.caixaFecharCaixa = new System.Windows.Forms.ToolStripMenuItem();
            this.caixaSuprimento = new System.Windows.Forms.ToolStripMenuItem();
            this.caixaSangria = new System.Windows.Forms.ToolStripMenuItem();
            this.venda = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastros = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastroProdutos = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastroClientes = new System.Windows.Forms.ToolStripMenuItem();
            this.financeiro = new System.Windows.Forms.ToolStripMenuItem();
            this.financeiroContasReceber = new System.Windows.Forms.ToolStripMenuItem();
            this.estoqueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.entradasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.estoqueBaixoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.posiçãoDoEstoqueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.menuPrincipal.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuPrincipal
            // 
            this.menuPrincipal.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.menuPrincipal.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.caixa,
            this.venda,
            this.cadastros,
            this.financeiro,
            this.estoqueToolStripMenuItem});
            this.menuPrincipal.Location = new System.Drawing.Point(0, 0);
            this.menuPrincipal.Name = "menuPrincipal";
            this.menuPrincipal.Size = new System.Drawing.Size(598, 29);
            this.menuPrincipal.TabIndex = 0;
            this.menuPrincipal.Text = "menuPrincipal";
            // 
            // caixa
            // 
            this.caixa.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.caixaAbrirCaixa,
            this.caixaFecharCaixa,
            this.caixaSuprimento,
            this.caixaSangria});
            this.caixa.Name = "caixa";
            this.caixa.Size = new System.Drawing.Size(59, 25);
            this.caixa.Text = "Caixa";
            // 
            // caixaAbrirCaixa
            // 
            this.caixaAbrirCaixa.Name = "caixaAbrirCaixa";
            this.caixaAbrirCaixa.Size = new System.Drawing.Size(167, 26);
            this.caixaAbrirCaixa.Text = "Abrir Caixa";
            this.caixaAbrirCaixa.Click += new System.EventHandler(this.caixaAbrirCaixa_Click);
            // 
            // caixaFecharCaixa
            // 
            this.caixaFecharCaixa.Name = "caixaFecharCaixa";
            this.caixaFecharCaixa.Size = new System.Drawing.Size(167, 26);
            this.caixaFecharCaixa.Text = "Fechar Caixa";
            this.caixaFecharCaixa.Click += new System.EventHandler(this.caixaFecharCaixa_Click);
            // 
            // caixaSuprimento
            // 
            this.caixaSuprimento.Name = "caixaSuprimento";
            this.caixaSuprimento.Size = new System.Drawing.Size(167, 26);
            this.caixaSuprimento.Text = "Suprimento";
            this.caixaSuprimento.Click += new System.EventHandler(this.caixaSuprimento_Click);
            // 
            // caixaSangria
            // 
            this.caixaSangria.Name = "caixaSangria";
            this.caixaSangria.Size = new System.Drawing.Size(167, 26);
            this.caixaSangria.Text = "Sangria";
            this.caixaSangria.Click += new System.EventHandler(this.caixaSangria_Click);
            // 
            // venda
            // 
            this.venda.Name = "venda";
            this.venda.Size = new System.Drawing.Size(65, 25);
            this.venda.Text = "Venda";
            this.venda.Click += new System.EventHandler(this.venda_Click);
            // 
            // cadastros
            // 
            this.cadastros.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastroProdutos,
            this.cadastroClientes});
            this.cadastros.Name = "cadastros";
            this.cadastros.Size = new System.Drawing.Size(91, 25);
            this.cadastros.Text = "Cadastros";
            // 
            // cadastroProdutos
            // 
            this.cadastroProdutos.Name = "cadastroProdutos";
            this.cadastroProdutos.Size = new System.Drawing.Size(143, 26);
            this.cadastroProdutos.Text = "Produtos";
            this.cadastroProdutos.Click += new System.EventHandler(this.cadastroProdutos_Click);
            // 
            // cadastroClientes
            // 
            this.cadastroClientes.Name = "cadastroClientes";
            this.cadastroClientes.Size = new System.Drawing.Size(143, 26);
            this.cadastroClientes.Text = "Clientes";
            this.cadastroClientes.Click += new System.EventHandler(this.cadastroClientes_Click);
            // 
            // financeiro
            // 
            this.financeiro.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.financeiroContasReceber});
            this.financeiro.Name = "financeiro";
            this.financeiro.Size = new System.Drawing.Size(94, 25);
            this.financeiro.Text = "Financeiro";
            // 
            // financeiroContasReceber
            // 
            this.financeiroContasReceber.Name = "financeiroContasReceber";
            this.financeiroContasReceber.Size = new System.Drawing.Size(200, 26);
            this.financeiroContasReceber.Text = "Contas a Receber";
            // 
            // estoqueToolStripMenuItem
            // 
            this.estoqueToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.entradasToolStripMenuItem,
            this.estoqueBaixoToolStripMenuItem,
            this.posiçãoDoEstoqueToolStripMenuItem});
            this.estoqueToolStripMenuItem.Name = "estoqueToolStripMenuItem";
            this.estoqueToolStripMenuItem.Size = new System.Drawing.Size(77, 25);
            this.estoqueToolStripMenuItem.Text = "Estoque";
            // 
            // entradasToolStripMenuItem
            // 
            this.entradasToolStripMenuItem.Name = "entradasToolStripMenuItem";
            this.entradasToolStripMenuItem.Size = new System.Drawing.Size(213, 26);
            this.entradasToolStripMenuItem.Text = "Entradas";
            // 
            // estoqueBaixoToolStripMenuItem
            // 
            this.estoqueBaixoToolStripMenuItem.Name = "estoqueBaixoToolStripMenuItem";
            this.estoqueBaixoToolStripMenuItem.Size = new System.Drawing.Size(213, 26);
            this.estoqueBaixoToolStripMenuItem.Text = "Lista de Reposição";
            // 
            // posiçãoDoEstoqueToolStripMenuItem
            // 
            this.posiçãoDoEstoqueToolStripMenuItem.Name = "posiçãoDoEstoqueToolStripMenuItem";
            this.posiçãoDoEstoqueToolStripMenuItem.Size = new System.Drawing.Size(213, 26);
            this.posiçãoDoEstoqueToolStripMenuItem.Text = "Posição do Estoque";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(12, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 22);
            this.label1.TabIndex = 1;
            this.label1.Text = "Caixa Fechado";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(146, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "label2";
            this.label2.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 29);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(598, 46);
            this.panel1.TabIndex = 3;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dataGridView1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 75);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(598, 259);
            this.panel2.TabIndex = 4;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.CausesValidation = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.ShowEditingIcon = false;
            this.dataGridView1.Size = new System.Drawing.Size(598, 259);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.TabStop = false;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(598, 334);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuPrincipal);
            this.MainMenuStrip = this.menuPrincipal;
            this.Name = "Main";
            this.Text = "EasyPDV";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Main_Load);
            this.menuPrincipal.ResumeLayout(false);
            this.menuPrincipal.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuPrincipal;
        private System.Windows.Forms.ToolStripMenuItem caixa;
        private System.Windows.Forms.ToolStripMenuItem caixaAbrirCaixa;
        private System.Windows.Forms.ToolStripMenuItem caixaFecharCaixa;
        private System.Windows.Forms.ToolStripMenuItem caixaSangria;
        private System.Windows.Forms.ToolStripMenuItem cadastros;
        private System.Windows.Forms.ToolStripMenuItem cadastroClientes;
        private System.Windows.Forms.ToolStripMenuItem cadastroProdutos;
        private System.Windows.Forms.ToolStripMenuItem venda;
        private System.Windows.Forms.ToolStripMenuItem caixaSuprimento;
        private System.Windows.Forms.ToolStripMenuItem financeiro;
        private System.Windows.Forms.ToolStripMenuItem financeiroContasReceber;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ToolStripMenuItem estoqueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem entradasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem estoqueBaixoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem posiçãoDoEstoqueToolStripMenuItem;
    }
}

