﻿namespace EasyPDV
{
    partial class CadastroProdutosEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.tbNomeProduto = new System.Windows.Forms.TextBox();
            this.tbFabricante = new System.Windows.Forms.TextBox();
            this.tbCodigoBarras = new System.Windows.Forms.TextBox();
            this.tbCustoMedio = new System.Windows.Forms.TextBox();
            this.tbUltimoCusto = new System.Windows.Forms.TextBox();
            this.tbPrecoVenda = new System.Windows.Forms.TextBox();
            this.tbEstoqueMinimo = new System.Windows.Forms.TextBox();
            this.tbEstoqueAtual = new System.Windows.Forms.TextBox();
            this.tbPercLucro = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "&Nome do Produto:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "&Fabricante:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(122, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Código de &Barras:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 124);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "C&usto Médio:";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 158);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(91, 17);
            this.label5.TabIndex = 8;
            this.label5.Text = "Últi&mo Custo:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 192);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(114, 17);
            this.label6.TabIndex = 10;
            this.label6.Text = "Preço de &Venda:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 260);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(112, 17);
            this.label7.TabIndex = 12;
            this.label7.Text = "&Estoque Mínimo:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 294);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 17);
            this.label8.TabIndex = 14;
            this.label8.Text = "Estoque &Atual:";
            // 
            // btnSalvar
            // 
            this.btnSalvar.BackColor = System.Drawing.Color.Green;
            this.btnSalvar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSalvar.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btnSalvar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalvar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalvar.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSalvar.Location = new System.Drawing.Point(385, 255);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(88, 26);
            this.btnSalvar.TabIndex = 16;
            this.btnSalvar.Text = "&Salvar";
            this.btnSalvar.UseVisualStyleBackColor = false;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.BackColor = System.Drawing.Color.Red;
            this.btnCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancelar.FlatAppearance.BorderColor = System.Drawing.Color.Red;
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.ForeColor = System.Drawing.Color.Gold;
            this.btnCancelar.Location = new System.Drawing.Point(504, 255);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(88, 26);
            this.btnCancelar.TabIndex = 17;
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // tbNomeProduto
            // 
            this.tbNomeProduto.Location = new System.Drawing.Point(157, 19);
            this.tbNomeProduto.Name = "tbNomeProduto";
            this.tbNomeProduto.Size = new System.Drawing.Size(435, 23);
            this.tbNomeProduto.TabIndex = 1;
            // 
            // tbFabricante
            // 
            this.tbFabricante.Location = new System.Drawing.Point(157, 53);
            this.tbFabricante.Name = "tbFabricante";
            this.tbFabricante.Size = new System.Drawing.Size(435, 23);
            this.tbFabricante.TabIndex = 3;
            // 
            // tbCodigoBarras
            // 
            this.tbCodigoBarras.Location = new System.Drawing.Point(157, 87);
            this.tbCodigoBarras.Name = "tbCodigoBarras";
            this.tbCodigoBarras.Size = new System.Drawing.Size(435, 23);
            this.tbCodigoBarras.TabIndex = 5;
            // 
            // tbCustoMedio
            // 
            this.tbCustoMedio.Location = new System.Drawing.Point(157, 121);
            this.tbCustoMedio.Name = "tbCustoMedio";
            this.tbCustoMedio.Size = new System.Drawing.Size(181, 23);
            this.tbCustoMedio.TabIndex = 7;
            // 
            // tbUltimoCusto
            // 
            this.tbUltimoCusto.Location = new System.Drawing.Point(157, 155);
            this.tbUltimoCusto.Name = "tbUltimoCusto";
            this.tbUltimoCusto.Size = new System.Drawing.Size(181, 23);
            this.tbUltimoCusto.TabIndex = 9;
            // 
            // tbPrecoVenda
            // 
            this.tbPrecoVenda.Location = new System.Drawing.Point(157, 189);
            this.tbPrecoVenda.Name = "tbPrecoVenda";
            this.tbPrecoVenda.Size = new System.Drawing.Size(181, 23);
            this.tbPrecoVenda.TabIndex = 11;
            // 
            // tbEstoqueMinimo
            // 
            this.tbEstoqueMinimo.Location = new System.Drawing.Point(157, 257);
            this.tbEstoqueMinimo.Name = "tbEstoqueMinimo";
            this.tbEstoqueMinimo.Size = new System.Drawing.Size(96, 23);
            this.tbEstoqueMinimo.TabIndex = 13;
            // 
            // tbEstoqueAtual
            // 
            this.tbEstoqueAtual.Location = new System.Drawing.Point(157, 291);
            this.tbEstoqueAtual.Name = "tbEstoqueAtual";
            this.tbEstoqueAtual.Size = new System.Drawing.Size(96, 23);
            this.tbEstoqueAtual.TabIndex = 15;
            // 
            // tbPercLucro
            // 
            this.tbPercLucro.Enabled = false;
            this.tbPercLucro.Location = new System.Drawing.Point(157, 223);
            this.tbPercLucro.Name = "tbPercLucro";
            this.tbPercLucro.ReadOnly = true;
            this.tbPercLucro.Size = new System.Drawing.Size(181, 23);
            this.tbPercLucro.TabIndex = 19;
            this.tbPercLucro.TabStop = false;
            this.tbPercLucro.Text = "0,00";
            this.tbPercLucro.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 226);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(140, 17);
            this.label9.TabIndex = 18;
            this.label9.Text = "Percentual de Lucro:";
            // 
            // CadastroProdutosEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(607, 330);
            this.Controls.Add(this.tbPercLucro);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.tbEstoqueAtual);
            this.Controls.Add(this.tbEstoqueMinimo);
            this.Controls.Add(this.tbPrecoVenda);
            this.Controls.Add(this.tbUltimoCusto);
            this.Controls.Add(this.tbCustoMedio);
            this.Controls.Add(this.tbCodigoBarras);
            this.Controls.Add(this.tbFabricante);
            this.Controls.Add(this.tbNomeProduto);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnSalvar);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "CadastroProdutosEdit";
            this.Text = "Incluir / Editar Produto";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.TextBox tbNomeProduto;
        private System.Windows.Forms.TextBox tbFabricante;
        private System.Windows.Forms.TextBox tbCodigoBarras;
        private System.Windows.Forms.TextBox tbCustoMedio;
        private System.Windows.Forms.TextBox tbUltimoCusto;
        private System.Windows.Forms.TextBox tbPrecoVenda;
        private System.Windows.Forms.TextBox tbEstoqueMinimo;
        private System.Windows.Forms.TextBox tbEstoqueAtual;
        private System.Windows.Forms.TextBox tbPercLucro;
        private System.Windows.Forms.Label label9;
    }
}