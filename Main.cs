﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Data;


namespace EasyPDV
{
    public partial class Main : Form
    {
        private Boolean caixaAberto = false;
        private Decimal valorCaixa = 0;
        private Decimal valorEntradas = 0;
        private Decimal valorSaidas = 0;
        private Decimal valorDinheiro = 0;
        private Decimal valorCheque = 0;
        private Decimal valorCartao = 0;
        private Decimal valorOutros = 0;
        private int IdCaixa = 0;
        private String msg = string.Empty;

        public Main()
        {
            InitializeComponent();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            DisconnectedOperations od = new DisconnectedOperations();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(" SELECT ");
            sb.AppendLine("     EhAberto, ");
            sb.AppendLine("     VrAbertura, ");
            sb.AppendLine("     VrEntradas, ");
            sb.AppendLine("     VrSaidas, ");
            sb.AppendLine("     IdCaixa ");
            sb.AppendLine(" FROM Caixa ");
            sb.AppendLine(" ORDER BY IdCaixa DESC ");
            sb.AppendLine(" LIMIT 1");

            try
            {
                DataTable dt = od.ExecuteSelectCommand(sb.ToString(), null, CommandType.Text);

                if (dt != null && dt.Rows.Count > 0)
                {
                    IdCaixa = Convert.ToInt32(dt.Rows[0]["IdCaixa"].ToString());

                    if (dt.Rows[0]["EhAberto"].ToString() == "1")
                    {
                        caixaAberto = true;
                        valorCaixa = Convert.ToDecimal(dt.Rows[0]["VrAbertura"].ToString());
                        valorEntradas = Convert.ToDecimal(dt.Rows[0]["VrEntradas"].ToString());
                        valorSaidas = Convert.ToDecimal(dt.Rows[0]["VrSaidas"].ToString());
                        SetLabelCaixaAberto();
                    }
                    else
                    {
                        caixaAberto = false;
                        SetLabelCaixaFechado();
                    }
                }
                else
                {
                    caixaAberto = false;
                    SetLabelCaixaFechado();
                }
            }
            catch
            {
                caixaAberto = false;
                SetLabelCaixaFechado();
            }
        }

        private void caixaAbrirCaixa_Click(object sender, EventArgs e)
        {
            AbrirCaixa frmAbrirCaixa = new AbrirCaixa();
            frmAbrirCaixa.AbrirCaixaUpdated += new AbrirCaixa.AbrirCaixaUpdateHandler(AbrirCaixa_Clicked);
            frmAbrirCaixa.Show();
        }

        private void caixaFecharCaixa_Click(object sender, EventArgs e)
        {
            FecharCaixa frmFecharCaixa = new FecharCaixa();
            frmFecharCaixa.FecharCaixaUpdated += new FecharCaixa.FecharCaixaUpdateHandler(FecharCaixa_Clicked);
            frmFecharCaixa.Show();
        }

        private void caixaSuprimento_Click(object sender, EventArgs e)
        {
            CaixaSuprimento frmCaixaSuprimento = new CaixaSuprimento();
            frmCaixaSuprimento.CaixaSuprimentoUpdated += new CaixaSuprimento.CaixaSuprimentoUpdateHandler(CaixaSuprimento_Clicked);
            frmCaixaSuprimento.Show();
        }

        private void caixaSangria_Click(object sender, EventArgs e)
        {
            CaixaSangria frmCaixaSangria = new CaixaSangria();
            frmCaixaSangria.CaixaSangriaUpdated += new CaixaSangria.CaixaSangriaUpdateHandler(CaixaSangria_Clicked);
            frmCaixaSangria.Show();
        }

        private void venda_Click(object sender, EventArgs e)
        {

        }

        private void cadastroProdutos_Click(object sender, EventArgs e)
        {
            CadastroProdutos frmCadastroProdutos = new CadastroProdutos();
            frmCadastroProdutos.Show();
        }

        private void cadastroClientes_Click(object sender, EventArgs e)
        {

        }

        public Boolean GetCaixaAberto()
        {
            return caixaAberto;
        }

        public void SetCaixaAberto(Boolean valor)
        {
            caixaAberto = valor;
        }

        public Decimal GetValorCaixa()
        {
            return valorCaixa;
        }

        public void SetValorCaixa(Decimal valor)
        {
            valorCaixa = valor;
        }

        public Decimal GetValorEntradas()
        {
            return valorEntradas;
        }

        public void SetValorEntradas(Decimal valor)
        {
            valorEntradas = valor;
        }

        public Decimal GetValorSaidas()
        {
            return valorSaidas;
        }

        public void SetValorSaidas(Decimal valor)
        {
            valorSaidas = valor;
        }

        public Label GetLabelCaixa()
        {
            return label1;
        }

        public void SetLabelCaixa(Label label)
        {
            label1 = label;
        }

        public ToolStripItem GetMenuVenda()
        {
            return menuPrincipal.Items.Find("venda", false)[0];
        }

        public void SetMenuVenda(ToolStripItem item)
        {
            menuPrincipal.Items.Find("venda", false)[0] = item;
        }

        public ToolStripItem GetMenuCaixaSangria()
        {
            return menuPrincipal.Items.Find("caixaSangria", true)[0];
        }

        public void SetMenuCaixaSangria(ToolStripItem item)
        {
            menuPrincipal.Items.Find("caixaSangria", true)[0] = item;
        }

        public ToolStripItem GetMenuCaixaSuprimento()
        {
            return menuPrincipal.Items.Find("caixaSuprimento", true)[0];
        }

        public void SetMenuCaixaSuprimento(ToolStripItem item)
        {
            menuPrincipal.Items.Find("caixaSuprimento", true)[0] = item;
        }

        public ToolStripItem GetMenuCaixaAbrirCaixa()
        {
            return menuPrincipal.Items.Find("caixaAbrirCaixa", true)[0];
        }

        public void SetMenuCaixaAbrirCaixa(ToolStripItem item)
        {
            menuPrincipal.Items.Find("caixaAbrirCaixa", true)[0] = item;
        }

        public ToolStripItem GetMenuCaixaFecharCaixa()
        {
            return menuPrincipal.Items.Find("caixaFecharCaixa", true)[0];
        }

        public void SetMenuCaixaFecharCaixa(ToolStripItem item)
        {
            menuPrincipal.Items.Find("caixaFecharCaixa", true)[0] = item;
        }

        public void SetLabelCaixaAberto()
        {
            Label labelCaixa = GetLabelCaixa();
            labelCaixa.ForeColor = Color.Green;
            labelCaixa.Text = "Caixa Aberto";
            SetLabelCaixa(labelCaixa);

            label2.Visible = true;
            label2.Text = "R$ " + (GetValorCaixa() + GetValorEntradas() - GetValorSaidas()).ToString("N");

            ToolStripItem item = GetMenuVenda();
            item.Enabled = true;
            SetMenuVenda(item);

            item = GetMenuCaixaAbrirCaixa();
            item.Enabled = false;
            SetMenuCaixaAbrirCaixa(item);

            item = GetMenuCaixaFecharCaixa();
            item.Enabled = true;
            SetMenuCaixaFecharCaixa(item);

            item = GetMenuCaixaSangria();
            item.Enabled = true;
            SetMenuCaixaSangria(item);

            item = GetMenuCaixaSuprimento();
            item.Enabled = true;
            SetMenuCaixaSuprimento(item);

            setGridviewDataSource();
        }

        public void SetLabelCaixaFechado()
        {
            Label labelCaixa = GetLabelCaixa();
            labelCaixa.ForeColor = Color.Red;
            labelCaixa.Text = "Caixa Fechado";
            SetLabelCaixa(labelCaixa);

            label2.Visible = false;

            ToolStripItem item = GetMenuVenda();
            item.Enabled = false;
            SetMenuVenda(item);

            item = GetMenuCaixaAbrirCaixa();
            item.Enabled = true;
            SetMenuCaixaAbrirCaixa(item);

            item = GetMenuCaixaFecharCaixa();
            item.Enabled = false;
            SetMenuCaixaFecharCaixa(item);

            item = GetMenuCaixaSangria();
            item.Enabled = false;
            SetMenuCaixaSangria(item);

            item = GetMenuCaixaSuprimento();
            item.Enabled = false;
            SetMenuCaixaSuprimento(item);

            setGridviewDataSource();
        }

        public void setGridviewDataSource()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(" SELECT ");
            sb.AppendLine("     IdCaixa AS ID, ");
            sb.AppendLine("     CASE EhAberto WHEN 1 THEN 'SIM' ELSE 'NÃO' END AS Aberto, ");
            sb.AppendLine("     DtCaixa AS Data, ");
            sb.AppendLine("     VrAbertura AS Abertura, ");
            sb.AppendLine("     VrEntradas AS Entradas, ");
            sb.AppendLine("     VrSaidas AS Saidas, ");
            sb.AppendLine("     VrFechamento AS Fechamento, ");
            sb.AppendLine("     VrDinheiro AS Dinheiro, ");
            sb.AppendLine("     VrCheque AS Cheque, ");
            sb.AppendLine("     VrCartao AS Cartão, ");
            sb.AppendLine("     VrOutros AS Outros ");
            sb.AppendLine(" FROM Caixa ");
            sb.AppendLine(" ORDER BY IdCaixa DESC");

            try
            {
                DataTable dt = new DisconnectedOperations().ExecuteSelectCommand(sb.ToString(), null, CommandType.Text);
                dataGridView1.DataSource = dt;
                var columns = dataGridView1.Columns;
                columns[0].Width = 30;
                columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                columns[1].Width = 50;
                columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                columns[9].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                columns[10].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private void AbrirCaixa_Clicked(object sender, AbrirCaixaUpdateEventArgs e)
        {
            caixaAberto = e.CaixaAberto;
            valorCaixa = e.ValorCaixa;
            valorEntradas = 0;
            valorSaidas = 0;

            if (e.CaixaAberto)
            {
                DisconnectedOperations od = new DisconnectedOperations();
                StringBuilder sb = new StringBuilder();
                sb.AppendLine(" INSERT INTO Caixa ");
                sb.AppendLine("     VALUES ( ");
                sb.AppendLine("         0, ");
                sb.AppendLine("         '" + DateTime.Now.ToString("yyyy-MM-dd") + "', ");
                sb.AppendLine("         " + valorCaixa.ToString().Replace(",", ".") + ", ");
                sb.AppendLine("         0, ");
                sb.AppendLine("         0, ");
                sb.AppendLine("         0, ");
                sb.AppendLine("         0, ");
                sb.AppendLine("         0, ");
                sb.AppendLine("         0, ");
                sb.AppendLine("         0, ");
                sb.AppendLine("         1 ");
                sb.AppendLine("     ) ");

                try
                {
                    IdCaixa = od.ExecuteCommand(sb.ToString(), null, CommandType.Text);
                    SetLabelCaixaAberto();
                }
                catch
                {
                    throw new Exception("Erro ao abrir o caixa !!!");
                }
            }
            else
            {
                SetLabelCaixaFechado();
                throw new Exception("Caixa já está aberto !!!");
            }
        }

        private void FecharCaixa_Clicked(object sender, FecharCaixaUpdateEventArgs e)
        {
            caixaAberto = e.CaixaAberto;
            valorDinheiro = e.ValorDinheiro;
            valorCheque = e.ValorCheque;
            valorCartao = e.ValorCartao;
            valorOutros = e.ValorOutros;

            if (!e.CaixaAberto)
            {
                DisconnectedOperations od = new DisconnectedOperations();
                Parameters p = new Parameters();
                StringBuilder sb = new StringBuilder();
                sb.Append(" UPDATE Caixa SET");
                sb.Append("     VrEntradas = ?VrEntradas, ");
                sb.Append("     VrSaidas = ?VrSaidas, ");
                sb.Append("     VrFechamento = ?VrFechamento, ");
                sb.Append("     VrDinheiro = ?VrDinheiro, ");
                sb.Append("     VrCheque = ?VrCheque, ");
                sb.Append("     VrCartao = ?VrCartao, ");
                sb.Append("     VrOutros = ?VrOutros, ");
                sb.Append("     EhAberto = ?EhAberto ");
                sb.Append(" WHERE IdCaixa = ?IdCaixa ");

                p.Add("VrEntradas", valorEntradas, 2);
                p.Add("VrSaidas", valorSaidas, 2);
                p.Add("VrFechamento", valorCaixa, 2);
                p.Add("VrDinheiro", valorDinheiro, 2);
                p.Add("VrCheque", valorCheque, 2);
                p.Add("VrCartao", valorCartao, 2);
                p.Add("VrOutros", valorOutros, 2);
                p.Add("EhAberto", 0);
                p.Add("IdCaixa", IdCaixa);

                try
                {
                    od.ExecuteCommand(sb.ToString(), p.ListParams, CommandType.Text);
                    IdCaixa = 0;
                    SetLabelCaixaFechado();
                }
                catch
                {
                    SetLabelCaixaAberto();
                    throw new Exception("Erro ao fechar o caixa !!!");
                }
            }
            else
            {
                SetLabelCaixaAberto();
                throw new Exception("Caixa já está fechado !!!");
            }
        }

        private void CaixaSuprimento_Clicked(object sender, CaixaSuprimentoUpdateEventArgs e)
        {
            valorEntradas += e.ValorEntradas;
            DisconnectedOperations od = new DisconnectedOperations();
            Parameters p = new Parameters();
            StringBuilder sb = new StringBuilder();
            sb.Append(" UPDATE Caixa SET");
            sb.Append("     VrEntradas = ?VrEntradas ");
            sb.Append(" WHERE IdCaixa = ?IdCaixa ");

            p.Add("VrEntradas", valorEntradas, 2);
            p.Add("IdCaixa", IdCaixa);

            try
            {
                od.ExecuteCommand(sb.ToString(), p.ListParams, CommandType.Text);
                SetLabelCaixaAberto();
            }
            catch
            {
                throw new Exception("Erro ao suprir o caixa !!!");
            }
        }

        private void CaixaSangria_Clicked(object sender, CaixaSangriaUpdateEventArgs e)
        {
            if (e.ValorSaidas <= (valorCaixa + valorEntradas - valorSaidas))
            {
                valorSaidas += e.ValorSaidas;
                DisconnectedOperations od = new DisconnectedOperations();
                Parameters p = new Parameters();
                StringBuilder sb = new StringBuilder();
                sb.Append(" UPDATE Caixa SET");
                sb.Append("     VrSaidas = ?VrSaidas ");
                sb.Append(" WHERE IdCaixa = ?IdCaixa ");

                p.Add("VrSaidas", valorSaidas, 2);
                p.Add("IdCaixa", IdCaixa);

                try
                {
                    od.ExecuteCommand(sb.ToString(), p.ListParams, CommandType.Text);
                    SetLabelCaixaAberto();
                }
                catch
                {
                    throw new Exception("Erro ao realizar sangria !!!");
                }
            }
            else
            {
                throw new Exception("Sangria maior que o valor do caixa !!!");
            }
        }
    }
}
