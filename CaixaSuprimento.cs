﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CommonLib;

namespace EasyPDV
{
    public partial class CaixaSuprimento : Form
    {
        public delegate void CaixaSuprimentoUpdateHandler(object sender, CaixaSuprimentoUpdateEventArgs e);
        public event CaixaSuprimentoUpdateHandler CaixaSuprimentoUpdated;
        private Common com = new Common();

        public CaixaSuprimento()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            decimal valor = Convert.ToDecimal("0" + tbValor.Text.Replace(".", "").Trim());
            if (valor == 0)
                MessageBox.Show("Informe o valor suprido ao caixa", "Erro", MessageBoxButtons.OK);
            else
            {
                try
                {
                    CaixaSuprimentoUpdateEventArgs args = new CaixaSuprimentoUpdateEventArgs(valor);
                    CaixaSuprimentoUpdated(this, args);
                    this.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK);
                }
            }
        }

        private void tbValor_Enter(object sender, EventArgs e)
        {
            MaskedTextBox mt = (MaskedTextBox)sender;
            mt = com.RestoreMask(mt);
        }

        private void tbValor_Leave(object sender, EventArgs e)
        {
            MaskedTextBox mt = (MaskedTextBox)sender;
            mt = com.CompleteMask(mt);
        }

        private void tbValor_KeyPress(object sender, KeyPressEventArgs e)
        {
            MaskedTextBox mt = (MaskedTextBox)sender;
            if (e.KeyChar != (char)Keys.Back && char.IsNumber(e.KeyChar))
            {
                mt = com.EnterNumericValue(mt, e.KeyChar);
            }
        }

        private void tbValor_KeyUp(object sender, KeyEventArgs e)
        {
            MaskedTextBox mt = (MaskedTextBox)sender;
            if (e.KeyCode == Keys.Back)
            {
                mt = com.BackSpace(mt);
            }
        }

    }
    public class CaixaSuprimentoUpdateEventArgs : System.EventArgs
    {
        private Decimal mValorEntradas;

        public CaixaSuprimentoUpdateEventArgs(Decimal sValorEntradas)
        {
            this.mValorEntradas = sValorEntradas;
        }

        public Decimal ValorEntradas
        {
            get { return mValorEntradas; }
        }
    }
}
