﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CommonLib;

namespace EasyPDV
{
    public partial class FecharCaixa : Form
    {
        public delegate void FecharCaixaUpdateHandler(object sender, FecharCaixaUpdateEventArgs e);
        public event FecharCaixaUpdateHandler FecharCaixaUpdated;
        private Common com = new Common();

        public FecharCaixa()
        {
            System.Windows.Forms.Form formMain = System.Windows.Forms.Application.OpenForms["Main"];
            InitializeComponent();
            lblVrAbertura.Text = ((Main)formMain).GetValorCaixa().ToString("N2");
            lblVrEntradas.Text = ((Main)formMain).GetValorEntradas().ToString("N2");
            lblVrSaidas.Text = ((Main)formMain).GetValorSaidas().ToString("N2");
            lblVrFinal.Text = (Convert.ToDecimal(lblVrAbertura.Text) +
                Convert.ToDecimal(lblVrEntradas.Text) -
                Convert.ToDecimal(lblVrSaidas.Text)).ToString("N2");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string valorDinheiro = tbVrDinheiro.Text.Replace(".", "").Trim();
            string valorCheque = tbVrCheques.Text.Replace(".", "").Trim();
            string valorCartao = tbVrCartoes.Text.Replace(".", "").Trim();
            string valorOutros = tbVrOutros.Text.Replace(".", "").Trim();
            string valorTotal = lblVrFinal2.Text.Replace(".", "").Trim();
            Boolean erro = false;

            if (String.IsNullOrEmpty(valorDinheiro))
            {
                MessageBox.Show("Informe o valor em dinheiro", "Erro", MessageBoxButtons.OK);
                erro=true;
            }

            if (String.IsNullOrEmpty(valorCheque))
            {
                MessageBox.Show("Informe o valor em cheques", "Erro", MessageBoxButtons.OK);
                erro = true;
            }

            if (String.IsNullOrEmpty(valorCartao))
            {
                MessageBox.Show("Informe o valor em cartões", "Erro", MessageBoxButtons.OK);
                erro = true;
            }

            if (String.IsNullOrEmpty(valorOutros))
            {
                MessageBox.Show("Informe o valor em outras formas de pagamento", "Erro", MessageBoxButtons.OK);
                erro = true;
            }

            if (String.IsNullOrEmpty(valorTotal) || valorTotal != lblVrFinal.Text.Replace(".","").Trim())
            {
                var ret = MessageBox.Show("Valor total diferente do valor de fechamento calculado. Fechar assim mesmo?", "Atenção", MessageBoxButtons.YesNoCancel);
                if (ret == DialogResult.Cancel || ret == DialogResult.No)
                    erro = true;
            }

            if (!erro)
            {
                try
                {
                    FecharCaixaUpdateEventArgs args = new FecharCaixaUpdateEventArgs(
                        Convert.ToDecimal(valorDinheiro), Convert.ToDecimal(valorCheque),
                        Convert.ToDecimal(valorCartao), Convert.ToDecimal(valorOutros), false);
                    FecharCaixaUpdated(this, args);
                    this.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK);
                }
            }
        }

        private void lblVrFinal2_TextChanged(object sender, EventArgs e)
        {
            lblVrDiferenca.Text = (Convert.ToDecimal(lblVrFinal.Text) -
                Convert.ToDecimal(lblVrFinal2.Text)).ToString("N2");
        }

        private void tbVrDinheiro_Enter(object sender, EventArgs e)
        {
            lblVrFinal2.Text = (Convert.ToDecimal("0" + lblVrFinal2.Text) -
                Convert.ToDecimal("0" + tbVrDinheiro.Text.Trim())).ToString("N2");
            MaskedTextBox mt = (MaskedTextBox)sender;
            mt = com.RestoreMask(mt);
        }

        private void tbVrDinheiro_Leave(object sender, EventArgs e)
        {
            lblVrFinal2.Text = (Convert.ToDecimal("0" + lblVrFinal2.Text) +
                Convert.ToDecimal("0" + tbVrDinheiro.Text.Trim())).ToString("N2");
            MaskedTextBox mt = (MaskedTextBox)sender;
            mt = com.CompleteMask(mt);
        }

        private void tbVrDinheiro_KeyPress(object sender, KeyPressEventArgs e)
        {
            MaskedTextBox mt = (MaskedTextBox)sender;
            if (e.KeyChar != (char)Keys.Back && char.IsNumber(e.KeyChar))
            {
                mt = com.EnterNumericValue(mt, e.KeyChar);
            }
        }

        private void tbVrDinheiro_KeyUp(object sender, KeyEventArgs e)
        {
            MaskedTextBox mt = (MaskedTextBox)sender;
            if (e.KeyCode == Keys.Back)
            {
                mt = com.BackSpace(mt);
            }
        }

        private void tbVrCheques_Enter(object sender, EventArgs e)
        {
            lblVrFinal2.Text = (Convert.ToDecimal("0" + lblVrFinal2.Text) -
                Convert.ToDecimal("0" + tbVrCheques.Text.Trim())).ToString("N2");
            MaskedTextBox mt = (MaskedTextBox)sender;
            mt = com.RestoreMask(mt);
        }

        private void tbVrCheques_Leave(object sender, EventArgs e)
        {
            lblVrFinal2.Text = (Convert.ToDecimal("0" + lblVrFinal2.Text) +
                Convert.ToDecimal("0" + tbVrCheques.Text.Trim())).ToString("N2");
            MaskedTextBox mt = (MaskedTextBox)sender;
            mt = com.CompleteMask(mt);
        }

        private void tbVrCheques_KeyPress(object sender, KeyPressEventArgs e)
        {
            MaskedTextBox mt = (MaskedTextBox)sender;
            if (e.KeyChar != (char)Keys.Back && char.IsNumber(e.KeyChar))
            {
                mt = com.EnterNumericValue(mt, e.KeyChar);
            }
        }

        private void tbVrCheques_KeyUp(object sender, KeyEventArgs e)
        {
            MaskedTextBox mt = (MaskedTextBox)sender;
            if (e.KeyCode == Keys.Back)
            {
                mt = com.BackSpace(mt);
            }
        }

        private void tbVrCartoes_Enter(object sender, EventArgs e)
        {
            lblVrFinal2.Text = (Convert.ToDecimal("0" + lblVrFinal2.Text) -
                Convert.ToDecimal("0" + tbVrCartoes.Text.Trim())).ToString("N2");
            MaskedTextBox mt = (MaskedTextBox)sender;
            mt = com.RestoreMask(mt);
        }

        private void tbVrCartoes_Leave(object sender, EventArgs e)
        {
            lblVrFinal2.Text = (Convert.ToDecimal("0" + lblVrFinal2.Text) +
                Convert.ToDecimal("0" + tbVrCartoes.Text.Trim())).ToString("N2");
            MaskedTextBox mt = (MaskedTextBox)sender;
            mt = com.CompleteMask(mt);
        }

        private void tbVrCartoes_KeyPress(object sender, KeyPressEventArgs e)
        {
            MaskedTextBox mt = (MaskedTextBox)sender;
            if (e.KeyChar != (char)Keys.Back && char.IsNumber(e.KeyChar))
            {
                mt = com.EnterNumericValue(mt, e.KeyChar);
            }
        }

        private void tbVrCartoes_KeyUp(object sender, KeyEventArgs e)
        {
            MaskedTextBox mt = (MaskedTextBox)sender;
            if (e.KeyCode == Keys.Back)
            {
                mt = com.BackSpace(mt);
            }
        }

        private void tbVrOutros_Enter(object sender, EventArgs e)
        {
            lblVrFinal2.Text = (Convert.ToDecimal("0" + lblVrFinal2.Text) -
                Convert.ToDecimal("0" + tbVrOutros.Text.Trim())).ToString("N2");
            MaskedTextBox mt = (MaskedTextBox)sender;
            mt = com.RestoreMask(mt);
        }

        private void tbVrOutros_Leave(object sender, EventArgs e)
        {
            lblVrFinal2.Text = (Convert.ToDecimal("0" + lblVrFinal2.Text) +
                Convert.ToDecimal("0" + tbVrOutros.Text.Trim())).ToString("N2");
            MaskedTextBox mt = (MaskedTextBox)sender;
            mt = com.CompleteMask(mt);
        }

        private void tbVrOutros_KeyPress(object sender, KeyPressEventArgs e)
        {
            MaskedTextBox mt = (MaskedTextBox)sender;
            if (e.KeyChar != (char)Keys.Back && char.IsNumber(e.KeyChar))
            {
                mt = com.EnterNumericValue(mt, e.KeyChar);
            }
        }

        private void tbVrOutros_KeyUp(object sender, KeyEventArgs e)
        {
            MaskedTextBox mt = (MaskedTextBox)sender;
            if (e.KeyCode == Keys.Back)
            {
                mt = com.BackSpace(mt);
            }
        }
    }

    public class FecharCaixaUpdateEventArgs : System.EventArgs
    {
        private Decimal mValorDinheiro;
        private Decimal mValorCheque;
        private Decimal mValorCartao;
        private Decimal mValorOutros;
        private Boolean mCaixaAberto;

        public FecharCaixaUpdateEventArgs(Decimal sValorDinheiro, Decimal sValorCheque ,
            Decimal sValorCartao, Decimal sValorOutros, Boolean sCaixaAberto)
        {
            this.mValorDinheiro = sValorDinheiro;
            this.mValorCheque = sValorCheque;
            this.mValorCartao = sValorCartao;
            this.mValorOutros = sValorOutros;
            this.mCaixaAberto = sCaixaAberto;
        }

        public Decimal ValorDinheiro
        { 
            get { return mValorDinheiro; } 
        }

        public Decimal ValorCheque
        {
            get { return mValorCheque; }
        }

        public Decimal ValorCartao
        {
            get { return mValorCartao; }
        }

        public Decimal ValorOutros
        {
            get { return mValorOutros; }
        }

        public Boolean CaixaAberto
        {
            get { return mCaixaAberto; }
        }
    }
}
