﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Windows.Forms;

namespace CommonLib
{
    public class Common
    {
        public Common ()
        {
        }

        public MaskedTextBox RestoreMask(MaskedTextBox mt)
        {
            mt.Mask = "999999999.99";
            string text = mt.Text.Replace(',', ' ').Trim();
            mt.Text = text.PadLeft(mt.Mask.Length - 1, ' ');
            mt.SelectionStart = mt.Mask.Length;
            mt.TextMaskFormat = MaskFormat.IncludePromptAndLiterals;
            return mt;
        }

        public MaskedTextBox CompleteMask(MaskedTextBox mt)
        {
            string dec = mt.Text.Substring(mt.Text.Length - 2, 2);
            string man = mt.Text.Substring(0, mt.Text.Length - 3).Trim();
            mt.Mask = "";
            int cont = 0;

            for (int i = man.Length; i > 0; i--)
            {
                if (man.Length > 3 && cont == 3)
                {
                    mt.Mask = "9," + mt.Mask;
                    cont = 1;
                }
                else
                {
                    mt.Mask = "9" + mt.Mask;
                    cont++;
                }
            }

            mt.Mask = mt.Mask + ".";
            int l = mt.Mask.Length;

            for (int i = l; i < (l + dec.Length); i++)
            {
                mt.Mask = mt.Mask + "9";
            }

            mt.Mask.Trim();
            mt.Text = (man.Trim() + dec).PadLeft(mt.Mask.Length - (man.Trim() + dec).Length + 2, ' ');

            return mt;
        }

        public MaskedTextBox EnterNumericValue(MaskedTextBox mt, char KeyChar)
        {
            String a = mt.Text + KeyChar;
            mt.Text = a.Substring(1, a.Length - 1);
            mt.SelectionStart = mt.Mask.Length + 1;

            return mt;
        }

        public MaskedTextBox BackSpace(MaskedTextBox mt)
        {
            mt.Text = " " + mt.Text;
            mt.SelectionStart = mt.Mask.Length + 1;
            return mt;
        }
    }
}
