﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Data;
using CommonLib;

namespace EasyPDV
{
    public partial class CadastroProdutos : Form
    {
        public delegate void CadastroProdutosUpdateHandler(object sender, CadastroProdutosUpdateEventArgs e);
        public event CadastroProdutosUpdateHandler CadastroProdutosUpdated;
        private Common com = new Common();

        public CadastroProdutos()
        {
            InitializeComponent();
        }

        private void CadastroProdutos_Load(object sender, EventArgs e)
        {
            setGridviewDataSource(null);
        }

        public void setGridviewDataSource(string Filter)
        {
            DataTable dt = new DataTable();
            Parameters p = new Parameters();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(" SELECT ");
            sb.AppendLine("     IdProduto as ID, ");
            sb.AppendLine("     NomeProduto as Nome, ");
            sb.AppendLine("     Fabricante as Fabricante, ");
            sb.AppendLine("     CustoMedio as Custo, ");
            sb.AppendLine("     PrecoVenda as Preço, ");
            sb.AppendLine("     PercLucro as Lucro, ");
            sb.AppendLine("     EstoqueAtual as Estoque, ");
            sb.AppendLine("     EstoqueMinimo as Mínimo ");
            sb.AppendLine(" FROM Produto ");
            sb.AppendLine(" WHERE 1=1 ");

            if (Filter != null && Filter != String.Empty)
            {
                string[] Filtro1 = Filter.Split('|');

                for (int i = 0; i < Filtro1.Length; i++)
                {
                    string[] Filtro2 = Filtro1[i].Split(':');
                    p.Add(Filtro2[0], Filtro2[1]);
                }

                foreach (string par in p.ListParams)
                {
                    if (par.Contains("Nome"))
                        sb.AppendLine("     AND NomeProduto LIKE CONCAT(CONCAT('%',?NomeProduto),'%') ");

                    if (par.Contains("Fabricante"))
                        sb.AppendLine("     AND Fabricante LIKE CONCAT(CONCAT('%',?Fabricante),'%') ");

                    if (par.Contains("Codigo"))
                        sb.AppendLine("     AND CodigoBarras LIKE CONCAT(CONCAT('%',?CodigoBarras),'%') ");
                }
            }

            try
            {
                dt = new DisconnectedOperations().ExecuteSelectCommand(sb.ToString(), p.ListParams, CommandType.Text);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }

            dataGridView1.DataSource = dt;
            var columns = dataGridView1.Columns;
            columns[0].Width = 50;
            columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            columns[1].Width = 200;
            columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            columns[2].Width = 200;
            columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        }

        private void btnFiltrar_Click(object sender, EventArgs e)
        {
            string Filtro = String.Empty;

            if ((tbNome.Text != null) && (tbNome.Text != String.Empty))
            {
                Filtro += "NomeProduto:" + tbNome.Text;
            }

            if ((tbFabricante.Text != null) && (tbFabricante.Text != String.Empty))
            {
                if (Filtro != String.Empty)
                    Filtro += "|";

                Filtro += "Fabricante:" + tbFabricante.Text;
            }

            if ((tbCodigoBarras.Text != null) && (tbCodigoBarras.Text != String.Empty))
            {
                if (Filtro != String.Empty)
                    Filtro += "|";

                Filtro += "CodigoBarras:" + tbCodigoBarras.Text;
            }

            setGridviewDataSource(Filtro);
        }

        private void CadastroProdutos_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F2:
                    btnEdit_Click(this, e);
                    break;
                case Keys.F3:
                    btnNew_Click(this, e);
                    break;
                case Keys.Delete:
                    btnDelete_Click(this, e);
                    break;
                default:
                    break;
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            CadastroProdutosEdit frm = new CadastroProdutosEdit();
            frm.Show();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {

        }

        private void CadastroProdutosSave_Clicked(object sender, CadastroProdutosUpdateEventArgs e)
        {
            //valorEntradas += e.ValorEntradas;
            DisconnectedOperations od = new DisconnectedOperations();
            Parameters p = new Parameters();
            StringBuilder sb = new StringBuilder();
            sb.Append(" UPDATE Caixa SET");
            sb.Append("     VrEntradas = ?VrEntradas ");
            sb.Append(" WHERE IdCaixa = ?IdCaixa ");

            //p.Add("VrEntradas", valorEntradas, 2);
            //p.Add("IdCaixa", IdCaixa);

            try
            {
                od.ExecuteCommand(sb.ToString(), p.ListParams, CommandType.Text);
            }
            catch
            {
                throw new Exception("Erro ao salvar produto !!!");
            }
        }
    }
}
