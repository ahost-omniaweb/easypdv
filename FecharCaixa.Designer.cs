﻿namespace EasyPDV
{
    partial class FecharCaixa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.lblVrAbertura = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblVrEntradas = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblVrSaidas = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblVrFinal = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblVrFinal2 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblVrDiferenca = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.tbVrDinheiro = new System.Windows.Forms.MaskedTextBox();
            this.tbVrCheques = new System.Windows.Forms.MaskedTextBox();
            this.tbVrCartoes = new System.Windows.Forms.MaskedTextBox();
            this.tbVrOutros = new System.Windows.Forms.MaskedTextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 34);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Valor Abertura:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // lblVrAbertura
            // 
            this.lblVrAbertura.Location = new System.Drawing.Point(186, 35);
            this.lblVrAbertura.Name = "lblVrAbertura";
            this.lblVrAbertura.Size = new System.Drawing.Size(100, 20);
            this.lblVrAbertura.TabIndex = 1;
            this.lblVrAbertura.Text = "VrAbertura";
            this.lblVrAbertura.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(58, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Entradas:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // lblVrEntradas
            // 
            this.lblVrEntradas.Location = new System.Drawing.Point(186, 66);
            this.lblVrEntradas.Name = "lblVrEntradas";
            this.lblVrEntradas.Size = new System.Drawing.Size(100, 20);
            this.lblVrEntradas.TabIndex = 3;
            this.lblVrEntradas.Text = "VrEntradas";
            this.lblVrEntradas.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(74, 98);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "Saídas:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // lblVrSaidas
            // 
            this.lblVrSaidas.Location = new System.Drawing.Point(186, 99);
            this.lblVrSaidas.Name = "lblVrSaidas";
            this.lblVrSaidas.Size = new System.Drawing.Size(100, 20);
            this.lblVrSaidas.TabIndex = 5;
            this.lblVrSaidas.Text = "VrSaídas";
            this.lblVrSaidas.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(48, 130);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 20);
            this.label4.TabIndex = 6;
            this.label4.Text = "Valor Final:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // lblVrFinal
            // 
            this.lblVrFinal.Location = new System.Drawing.Point(186, 131);
            this.lblVrFinal.Name = "lblVrFinal";
            this.lblVrFinal.Size = new System.Drawing.Size(100, 20);
            this.lblVrFinal.TabIndex = 7;
            this.lblVrFinal.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(23, 192);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(113, 20);
            this.label5.TabIndex = 8;
            this.label5.Text = "Valor Dinheiro:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(18, 225);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(118, 20);
            this.label6.TabIndex = 11;
            this.label6.Text = "Valor Cheques:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(26, 258);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(110, 20);
            this.label7.TabIndex = 12;
            this.label7.Text = "Valor Cartões:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(47, 324);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(89, 20);
            this.label8.TabIndex = 16;
            this.label8.Text = "Valor Total:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // lblVrFinal2
            // 
            this.lblVrFinal2.BackColor = System.Drawing.SystemColors.Window;
            this.lblVrFinal2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblVrFinal2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.lblVrFinal2.Location = new System.Drawing.Point(186, 321);
            this.lblVrFinal2.Name = "lblVrFinal2";
            this.lblVrFinal2.Size = new System.Drawing.Size(100, 26);
            this.lblVrFinal2.TabIndex = 17;
            this.lblVrFinal2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblVrFinal2.TextChanged += new System.EventHandler(this.lblVrFinal2_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(54, 389);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(82, 20);
            this.label9.TabIndex = 18;
            this.label9.Text = "Diferença:";
            // 
            // lblVrDiferenca
            // 
            this.lblVrDiferenca.BackColor = System.Drawing.SystemColors.Window;
            this.lblVrDiferenca.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblVrDiferenca.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.lblVrDiferenca.Location = new System.Drawing.Point(186, 386);
            this.lblVrDiferenca.Name = "lblVrDiferenca";
            this.lblVrDiferenca.Size = new System.Drawing.Size(100, 26);
            this.lblVrDiferenca.TabIndex = 19;
            this.lblVrDiferenca.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Green;
            this.button1.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.button1.Location = new System.Drawing.Point(131, 438);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 36);
            this.button1.TabIndex = 20;
            this.button1.Text = "&Fechar";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(34, 291);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(102, 20);
            this.label10.TabIndex = 14;
            this.label10.Text = "Valor Outros:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // tbVrDinheiro
            // 
            this.tbVrDinheiro.HidePromptOnLeave = true;
            this.tbVrDinheiro.HideSelection = false;
            this.tbVrDinheiro.Location = new System.Drawing.Point(186, 189);
            this.tbVrDinheiro.Name = "tbVrDinheiro";
            this.tbVrDinheiro.PromptChar = ' ';
            this.tbVrDinheiro.Size = new System.Drawing.Size(100, 26);
            this.tbVrDinheiro.TabIndex = 24;
            this.tbVrDinheiro.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbVrDinheiro.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePrompt;
            this.tbVrDinheiro.Enter += new System.EventHandler(this.tbVrDinheiro_Enter);
            this.tbVrDinheiro.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbVrDinheiro_KeyPress);
            this.tbVrDinheiro.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tbVrDinheiro_KeyUp);
            this.tbVrDinheiro.Leave += new System.EventHandler(this.tbVrDinheiro_Leave);
            // 
            // tbVrCheques
            // 
            this.tbVrCheques.HidePromptOnLeave = true;
            this.tbVrCheques.HideSelection = false;
            this.tbVrCheques.Location = new System.Drawing.Point(186, 222);
            this.tbVrCheques.Name = "tbVrCheques";
            this.tbVrCheques.PromptChar = ' ';
            this.tbVrCheques.Size = new System.Drawing.Size(100, 26);
            this.tbVrCheques.TabIndex = 25;
            this.tbVrCheques.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbVrCheques.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePrompt;
            this.tbVrCheques.Enter += new System.EventHandler(this.tbVrCheques_Enter);
            this.tbVrCheques.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbVrCheques_KeyPress);
            this.tbVrCheques.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tbVrCheques_KeyUp);
            this.tbVrCheques.Leave += new System.EventHandler(this.tbVrCheques_Leave);
            // 
            // tbVrCartoes
            // 
            this.tbVrCartoes.HidePromptOnLeave = true;
            this.tbVrCartoes.HideSelection = false;
            this.tbVrCartoes.Location = new System.Drawing.Point(186, 255);
            this.tbVrCartoes.Name = "tbVrCartoes";
            this.tbVrCartoes.PromptChar = ' ';
            this.tbVrCartoes.Size = new System.Drawing.Size(100, 26);
            this.tbVrCartoes.TabIndex = 26;
            this.tbVrCartoes.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbVrCartoes.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePrompt;
            this.tbVrCartoes.Enter += new System.EventHandler(this.tbVrCartoes_Enter);
            this.tbVrCartoes.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbVrCartoes_KeyPress);
            this.tbVrCartoes.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tbVrCartoes_KeyUp);
            this.tbVrCartoes.Leave += new System.EventHandler(this.tbVrCartoes_Leave);
            // 
            // tbVrOutros
            // 
            this.tbVrOutros.HidePromptOnLeave = true;
            this.tbVrOutros.HideSelection = false;
            this.tbVrOutros.Location = new System.Drawing.Point(186, 288);
            this.tbVrOutros.Name = "tbVrOutros";
            this.tbVrOutros.PromptChar = ' ';
            this.tbVrOutros.Size = new System.Drawing.Size(100, 26);
            this.tbVrOutros.TabIndex = 27;
            this.tbVrOutros.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbVrOutros.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePrompt;
            this.tbVrOutros.Enter += new System.EventHandler(this.tbVrOutros_Enter);
            this.tbVrOutros.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbVrOutros_KeyPress);
            this.tbVrOutros.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tbVrOutros_KeyUp);
            this.tbVrOutros.Leave += new System.EventHandler(this.tbVrOutros_Leave);
            // 
            // FecharCaixa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(326, 491);
            this.Controls.Add(this.tbVrOutros);
            this.Controls.Add(this.tbVrCartoes);
            this.Controls.Add(this.tbVrCheques);
            this.Controls.Add(this.tbVrDinheiro);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lblVrDiferenca);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.lblVrFinal2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblVrFinal);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblVrSaidas);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblVrEntradas);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblVrAbertura);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "FecharCaixa";
            this.Text = "FecharCaixa";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblVrAbertura;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblVrEntradas;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblVrSaidas;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblVrFinal;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblVrFinal2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblVrDiferenca;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.MaskedTextBox tbVrDinheiro;
        private System.Windows.Forms.MaskedTextBox tbVrCheques;
        private System.Windows.Forms.MaskedTextBox tbVrCartoes;
        private System.Windows.Forms.MaskedTextBox tbVrOutros;
    }
}